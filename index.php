<?php

include_once("vendor/autoload.php");

use App\seip\ID134158\Module\About\About;
use App\seip\ID134158\Module\Contact\Contact;
use App\seip\ID134158\Module\Image\Image;
use App\seip\ID134158\Module\Profile\Profile;
use App\seip\ID134158\Module\Users\Users;

$mAbout= new About('Satej');
$mContact = new Contact('01857209157');
$mImage = new Image('0');
$mProfile = new Profile('Rangamati');
$mUser = new Users('Dhaka');

